# `project` - automate initial project setup.

## Why

Whenever I needed to setup new *Go* project I found myself copy/pasting from previous one. So I created this tool
that allows me to prepare different project *template* repositories and generate new project based on required template.

# How to use

## Install

Either download binary from [here](https://bitbucket.org/nildev/tools/downloads) or `go get`
```
go get bitbucket.org/nildev/tools/cmd/project
```

## Project templates

Using Golang template syntax prepare your project template, for example take a look at [this one](https://bitbucket.org/nildev/ride-service-template).
Then create `project.json` file with actual values for each variable you have defined in your project template.

## Generate new project

- replace $PATH_TO_NEW_PROJECT with path to directory where newly generated project should be created
- replace $PATH_TO_CONFIG_JSON with path to json file with values for variables defined in template 
- replace $PATH_TO_TEMPLATE_REPO to project template git repo for example `git@bitbucket.org:nildev/ride-service-template.git`

Run:
```
project setup --destDir=$PATH_TO_NEW_PROJECT --configFile=$PATH_TO_CONFIG_JSON --templateRepo="$PATH_TO_TEMPLATE_REPO"
```

# Available templates

Here is a list of available templates.
If you have created one please do a pull request with link to that repo.

 * bitbucket.org:nildev/ride-service-template
 

# How to setup for development

1) Get repo

```
go get bitbucket.org/nildev/tools
```

2) Restore deps

```
godep restore 
```

3) Run `build`

```
./build
```