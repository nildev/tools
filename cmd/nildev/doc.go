/*
`codegen` command allows you to perform various operations related to code generation.

     $ go get bitbucket.org/nildev/tools/cmd/codegen
     $ codegen --help
*/
package main
